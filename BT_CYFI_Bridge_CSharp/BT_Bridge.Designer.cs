﻿namespace BT_CYFI_Bridge_CSharp
{
    partial class BT_Bridge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.blE_UserControl1 = new BT_CYFI_Bridge_CSharp.BLE_UserControl();
            this.blE_UserControl04 = new BT_CYFI_Bridge_CSharp.BLE_UserControl();
            this.blE_UserControl02 = new BT_CYFI_Bridge_CSharp.BLE_UserControl();
            this.blE_UserControl01 = new BT_CYFI_Bridge_CSharp.BLE_UserControl();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-39, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Device";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.blE_UserControl1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.blE_UserControl04, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.blE_UserControl02, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.blE_UserControl01, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1425, 670);
            this.tableLayoutPanel1.TabIndex = 32;
            // 
            // blE_UserControl1
            // 
            this.blE_UserControl1.BLE_NodeNumber = -1;
            this.blE_UserControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.blE_UserControl1.Location = new System.Drawing.Point(715, 3);
            this.blE_UserControl1.Name = "blE_UserControl1";
            this.blE_UserControl1.Size = new System.Drawing.Size(350, 664);
            this.blE_UserControl1.TabIndex = 35;
            this.blE_UserControl1.BLE_InitOnClicked += new BT_CYFI_Bridge_CSharp.BLE_InitOnClickedEventHander(this.blE_UserControl1_BLE_InitOnClicked);
            // 
            // blE_UserControl04
            // 
            this.blE_UserControl04.BLE_NodeNumber = -1;
            this.blE_UserControl04.Dock = System.Windows.Forms.DockStyle.Fill;
            this.blE_UserControl04.Location = new System.Drawing.Point(359, 3);
            this.blE_UserControl04.Name = "blE_UserControl04";
            this.blE_UserControl04.Size = new System.Drawing.Size(350, 664);
            this.blE_UserControl04.TabIndex = 34;
            this.blE_UserControl04.BLE_InitOnClicked += new BT_CYFI_Bridge_CSharp.BLE_InitOnClickedEventHander(this.blE_UserControl04_BLE_InitOnClicked);
            // 
            // blE_UserControl02
            // 
            this.blE_UserControl02.BLE_NodeNumber = -1;
            this.blE_UserControl02.Dock = System.Windows.Forms.DockStyle.Fill;
            this.blE_UserControl02.Location = new System.Drawing.Point(1071, 3);
            this.blE_UserControl02.Name = "blE_UserControl02";
            this.blE_UserControl02.Size = new System.Drawing.Size(351, 664);
            this.blE_UserControl02.TabIndex = 33;
            this.blE_UserControl02.BLE_InitOnClicked += new BT_CYFI_Bridge_CSharp.BLE_InitOnClickedEventHander(this.blE_UserControl02_BLE_InitOnClicked);
            // 
            // blE_UserControl01
            // 
            this.blE_UserControl01.BLE_NodeNumber = -1;
            this.blE_UserControl01.Dock = System.Windows.Forms.DockStyle.Fill;
            this.blE_UserControl01.Location = new System.Drawing.Point(3, 3);
            this.blE_UserControl01.Name = "blE_UserControl01";
            this.blE_UserControl01.Size = new System.Drawing.Size(350, 664);
            this.blE_UserControl01.TabIndex = 32;
            this.blE_UserControl01.BLE_InitOnClicked += new BT_CYFI_Bridge_CSharp.BLE_InitOnClickedEventHander(this.blE_UserControl01_BLE_InitOnClicked);
            // 
            // BT_Bridge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1425, 670);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label1);
            this.Name = "BT_Bridge";
            this.Text = "MyOVisionBLEDemo (C#)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BT_Bridge_FormClosing);
            this.Load += new System.EventHandler(this.BT_Bridge_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private BLE_UserControl blE_UserControl01;
        private BLE_UserControl blE_UserControl02;
        private BLE_UserControl blE_UserControl1;
        private BLE_UserControl blE_UserControl04;
    }
}

