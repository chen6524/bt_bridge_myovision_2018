﻿using BLE_Interface;
using BT_CYFI_Bridge_CSharp.Properties;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;


namespace BT_CYFI_Bridge_CSharp
{

    public partial class BT_Bridge : Form
    {

        List<String> bleAddr_s = new List<String>();
        List<String> bleName_s = new List<String>();
        List<BLE_UserControl> BLE_UserControls = new List<BLE_UserControl>();


        //BLE_Interface_Form mBLEInterfaceEx = new BLE_Interface_Form();	

        public BT_Bridge()
        {
            InitializeComponent();
        }

        private void BT_Bridge_Load(object sender, EventArgs e)
        {
        }

         private void BT_Bridge_FormClosing(object sender, FormClosingEventArgs e)
         {
         }

         private void blE_UserControl01_BLE_InitOnClicked(object sender, BLEControlOnClickedArgs args)
         {
             int bleNumer = args.BLE_Number;

             if(bleAddr_s.Count >= bleNumer + 1)
             {
                 BLE_UserControls[bleNumer].BLE_Addr = bleAddr_s[bleNumer];
                 blE_UserControl01.BLE_Addr = bleAddr_s[bleNumer];
             }
             else
             {
                 BLE_UserControls[bleNumer].BLE_Addr = "The BLE not Exist!";
                 blE_UserControl01.BLE_Addr = "The BLE not Exist!";
             }
         }

        private void blE_UserControl02_BLE_InitOnClicked(object sender, BLEControlOnClickedArgs args)
        {
            int bleNumer = args.BLE_Number;
        }

        private void blE_UserControl1_BLE_InitOnClicked(object sender, BLEControlOnClickedArgs args)
        {
            int bleNumer = args.BLE_Number;
        }

        private void blE_UserControl04_BLE_InitOnClicked(object sender, BLEControlOnClickedArgs args)
        {
            int bleNumer = args.BLE_Number;
        }
    }
}
