﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using BT_CYFI_Bridge_CSharp.Properties;

using BLE_Interface;

namespace BT_CYFI_Bridge_CSharp
{
    public partial class BLE_UserControl : UserControl
    {
        #region Privates
        //byte[] sendBuff = new byte[64];
        private int BLE_number;
        //Timer mTimer = new Timer(new TimerCallback(callTimer()));
        #endregion

        public event BLE_InitOnClickedEventHander BLE_InitOnClicked;
        //public event BLE_DataModeOnClickedEventHandler BLE_DataModeOnClicked;
        //public event BLE_StartInOnClickedEventHander BLE_StartInOnClicked;
        //public event BLE_SendDataOnClickedEventHander BLE_SendDataOnClicked;
        //public event BLE_KeepSendingOnClickedEventHander BLE_KeepSendingOnClicked;

        private BLE_Interface_Form mBLEInterface = new BLE_Interface_Form();
        //public AutoResetEvent oSignalEvent = new AutoResetEvent(false);

        //int m_OutstandingFrames = 0;
        //bool m_Stop;
        byte led0;
        byte led1;

        List<String> bleAddr_s = new List<String>();
        List<String> bleName_s = new List<String>();

        ThreadStart send_cmd_job;
        Thread send_thread;

        public BLE_UserControl()
        {
            InitializeComponent();
            BLE_number = -1;

            //sendBuff[0] = (byte)' ';
            //for (int i = 0; i < 63; i++)
            //{
            //    sendBuff[i + 1] = (byte)(sendBuff[i] + 1);
            //}

            //mSendBytes.Text = "01 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f 10 11 12 13 14";

            send_cmd_job = new ThreadStart(cmdSend);

            if (!mBLEInterface.IsBLERadioExist())
            {
                mReceivedBytes.AppendText("Bluetooth radio is not present or not functioning.  Please plug in the dongle and try again.\r\n");
            }
            //Could need to add kernel32.lib user32.lib advapi32.lib into Linker inputs option!
            // This application supports Microsoft on Win8				 
            if (!mBLEInterface.IsOSWin8(true))
                mReceivedBytes.AppendText("The application just can be running in Windows 8 or later.\r\n");


            if (mBLEInterface.GetDeviceList(out bleAddr_s, out bleName_s) > 0)
            {
                //int id = 0;
                foreach (String addr in bleAddr_s)
                {
                    //mBT_Addr.Text = addr;//new String(bleAddr));
                    //blE_UserControl01.BLE_NodeNumber = 0;
                    //BLE_UserControls.Add(blE_UserControl01);
                    mBT_Addr.Items.Add(addr);
                }
                foreach (String name in bleName_s)
                {
                    mBT_Name.Items.Add(name);//new String(bleAddr));
                }
                //mBT_Addr.SelectedIndex = 0;
                //mBT_Name.SelectedIndex = 0;
                //mSend.Enabled = true;
                //mSendOut.Enabled = true;

            }
            if (mBT_Addr.Items.Count > 0)
                mBT_Addr.SelectedIndex = mBT_Name.SelectedIndex = 0;
        }
     
        void Close()
        {
            //oSignalEvent.Close();
            mBLEInterface.Close();
        }

        #region public_declare
#if true
        public int BLE_NodeNumber
        {
            set
            { 
                if (value != BLE_number)
                {
                    BLE_number = value;
                    mInitBLE.Text = "Init BLE" + (BLE_number+1).ToString("d2");

                }
            }
            get
            {
                return BLE_number;
            }
        }

        public string BLE_Addr
        {
            set
            {
                if (value != mBT_Addr.Text)
                {
                    mBT_Addr.Text = value;
                    mInitBLE.Enabled = false;
                }
            }
        }

        //public string SendBytes
        //{
        //    set
        //    {
        //        if (value != mSendBytes.Text)
        //        {
        //            mSendBytes.Text = value;
        //        }
        //    }
        //}

        public string ReceivedBytes
        {
            set
            {
                //if (value != mSendBytes.Text)
                {
                    mReceivedBytes.AppendText(value);
                }
            }
        }

        public void ClearAll()
        {
            mReceivedBytes.Clear();
            textBoxACK.Clear();
            m_receivedBytes = 0;
            //m_receivedPackets = 0;
            //m_receivedPacket_Count = 0;
            mAckPackets = 0;
        }
#endif
        #endregion


        private void AccessForm(String action, String formText)
        {
            try
            {
                //  Select an action to perform on the form:
                switch (action)
                {
                    case "RECV_BYTES":
                        mReceivedPackets.Text = formText;
                        break;
                    case "ACKED":
                        textBoxACK.AppendText("<--" + formText);    
                        //if(mReceivedBytes.line)
                        break;
                    case "RECV":
                        mReceivedBytes.AppendText(formText);
                        //if(mReceivedBytes.line)
                        break;
                    case "PACKET":
                        //mReceivedPackets.AppendText(formText);
                        //if(mReceivedBytes.line)
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                //DisplayException(this.Name, ex);
                MessageBox.Show("AccessForm()" + ex.Message);
            }

           // Application.DoEvents();
        }


        private delegate void MarshalToForm(String action, String textToAdd);//, System.Drawing.Color color, ref byte[] parameter);

        private void MyMarshalToForm(String action, String textToDisplay)
        {
            object[] args = { action, textToDisplay };
            MarshalToForm MarshalToFormDelegate = null;

            //  The AccessForm routine contains the code that accesses the form.
            MarshalToFormDelegate = new MarshalToForm(AccessForm);

            //  Execute AccessForm, passing the parameters in args.
            base.Invoke(MarshalToFormDelegate, args);
        }

        private byte DecodeToDec(ref byte[] dec_in, int len, out byte[] hex_out)
        {
            byte i, j;
            byte len2 = 0;
            ushort tmpH = 0, tmpL = 0;

            if (len % 2 != 0)
                len2 = (byte)(len + 1);
            else
                len2 = (byte)len;

            hex_out = new byte[len2 / 2];

            j = 0;
            for (i = 0; i < len / 2; i++)
            {
                if ((dec_in[j] >= '0') && (dec_in[j] <= '9'))
                    tmpH = (byte)(dec_in[j] - '0');
                else
                {
                    if ((dec_in[j] >= 'A') && (dec_in[j] <= 'F'))
                        tmpH = (byte)(dec_in[j] - 'A' + 10);
                    else if ((dec_in[j] >= 'a') && (dec_in[j] <= 'f'))
                        tmpH = (byte)(dec_in[j] - 'a' + 10);
                }
                j++;
                if ((dec_in[j] >= '0') && (dec_in[j] <= '9'))
                    tmpL = (byte)(dec_in[j] - '0');
                else
                {
                    if ((dec_in[j] >= 'A') && (dec_in[j] <= 'F'))
                        tmpL = (byte)(dec_in[j] - 'A' + 10);
                    else if ((dec_in[j] >= 'a') && (dec_in[j] <= 'f'))
                        tmpL = (byte)(dec_in[j] - 'a' + 10);
                }
                j++;

                hex_out[i] = (byte)(tmpH * 16 + tmpL);
            }
            i = (byte)(len2 / 2);
            return i;
        }

        int m_receivedBytes = 0;
        //int m_receivedPacket_Count = 0;
        //int m_receivedPackets = 0;
        int mAckPackets = 0;
        int now = 0;//256 - (0x7E - 0x20 + 1);
        int then = 0;
        int dif = 0, oldif = 0;

        //const byte CHUNK_BINDING = 0;      // no packet transmission
        //const byte CHUNK_SHELF_MODE_U = 1;      // packet size will be  2
        //const byte CHUNK_SHELF_MODE_D = 4;      // packet size will be  5
        //const byte CHUNK_POWER_OFF = 10;     // packet size will be 11
        //const byte CHUNK_POWER_ON = 11;     // packet size will be 12
        //const byte CHUNK_IMMEDIATE = 12;     // packet size will be 13
        //const byte CHUNK_HIT_DATA = 13;     // packet size will be 14
        //bool FrameRecv_disable = false;
        float fBV = 0;
        string sBV_last = "";
        int dif_average = 0;
        int m_receivedCount = 0;
        string difS = "xxx";
        string difS_A = "xxx";
        bool FrameReceived(BLE_ReceiveEventArgs args)
        {
            //BTW_GATT_VALUE pValue = new BTW_GATT_VALUE();
            //byte[] inBuffer = args.inBuffer;
            

            now = Environment.TickCount;// &Int32.MaxValue; 
            dif = now - then;
            if (m_receivedBytes > 10)
            {                
                //if (dif > 100)
                //    dif = 64;
                difS = dif.ToString("d3");                

                m_receivedCount++;
                dif_average += dif;
                if( m_receivedCount >= 25)
                {
                    m_receivedCount = 0;
                    dif_average = dif_average / 25;
                    difS_A = dif_average.ToString("") + " ms";
                    dif_average = 0;
                }
            }
            else
            {
                difS = "064";
            }

            then = now;

            byte[] buff = args.inBuffer;
            int lenX = args.length;

            if (lenX == 4)
            {
                String Str1 = Encoding.ASCII.GetString(buff);
                MyMarshalToForm("RECV", Str1 + "\r\n");
                return true;
            }

                //Thread.Sleep(0);

                byte[] rawBuff;// = new byte[lenX / 2];
                ushort BattVolt = 0;
                double Voltage = 0.0;
                //float fBV = 0;
                string sBV = "";
                byte deviceType = 0;
                string DeviceAB = "";
                string DeviceName = "";

                DecodeToDec(ref buff, lenX, out rawBuff);
                deviceType = (byte)(rawBuff[0] & 0x0F);
                if ((deviceType == 3) || ((deviceType == 2)))
                {
                    DeviceAB = ((rawBuff[0] & 0x80) == 0x80) ? "B" : "A";
                    DeviceName = (deviceType == 3) ? "Scan" : "Dyna";
                    DeviceName = DeviceName + DeviceAB;

                    if ((rawBuff[1] & 0x10) == 0x00)
                    {
                        BattVolt = (ushort)((rawBuff[6] << 4) | (rawBuff[7] >> 4));
                        Voltage = (double)BattVolt * (double)0.001221;
                        fBV = (float)Voltage;
                        sBV = string.Format("{0:f2}V,", fBV) + DeviceName;
                        sBV_last = sBV;
                    }
                    else
                    {
                        sBV = sBV_last;
                    }
                }
                else if (deviceType == 1)
                {
                    DeviceAB = ((rawBuff[0] & 0x80) == 0x80) ? "FlexB" : "FlexA";
                    BattVolt = rawBuff[2];
                    Voltage = (double)BattVolt * (double)(0.0235);
                    fBV = (float)Voltage;
                    sBV = string.Format("{0:f2}V, ", fBV) + DeviceAB;
                }
                else
                    return true;

            m_receivedBytes++;

            String Str = Encoding.ASCII.GetString(buff);
            MyMarshalToForm("RECV", difS + "  " + Str + "    { " + sBV + " } " + "\r\n");
            if (lenX > 8)
                mReceiveBox.Text = difS_A + "     " + Str;
            MyMarshalToForm("RECV_BYTES", m_receivedBytes.ToString());

            return true;
        }

        void BLE_OnBLEAckReceived(Object sender, BLE_ReceiveEventArgs args)
        {
            byte[] buff = args.inBuffer;
            int lenX = args.length;

            string time = DateTime.Now.ToLongTimeString();

            String Indi = Encoding.ASCII.GetString(buff);
            String Str = Indi + (++mAckPackets).ToString("d4");            
            if (Indi == "ST")
            {
                MyMarshalToForm("ACKED", Str + "\r\n");
                mReceivedBytes.AppendText("\r\n  BLE Powered off!........" + time + "\r\n");
                //mReceivedBytes.AppendText("\r\n  You have to re-initialize BLE \r\n   if you want to receve message again!\r\n");
            }
            else if (Indi == "RS")
            {
                MyMarshalToForm("ACKED", Str + "\r\n");
                mBLEInterface.Data_SetDescriptors(1, 1);
                mBLEInterface.SendCommand((byte)'B', 1);
                mReceivedBytes.AppendText("\r\n  BLE Connected!.........." + time + "\r\n\r\n");
                // mBLEInterface.RegisterNotification();
            }
            else
            {
                MyMarshalToForm("ACKED", Str + "\r\n");
            }

                //m_OutstandingFrames--;
                //oSignalEvent.Set();
        }


        void BLE_OnBLEDataReceived(Object sender, BLE_ReceiveEventArgs args)
        {
            if (InvokeRequired)
            {
                Invoke(new BLE_ReceiveEventHandler(BLE_OnBLEDataReceived), new Object[] { sender, args });
            }
            else
            {
                FrameReceived(args);
            }
        }

        private void mInitBLE_Click(object sender, EventArgs e)
        {
            if (BLE_InitOnClicked != null)
            {
                //BLE_InitOnClicked(this, new BLEControlOnClickedArgs(BLE_number));
                if (mBT_Addr.Text.Length == 12)
                {
                    if(mBT_Addr.SelectedIndex >= 0)
                    if (mBLEInterface.InitTest(mBT_Addr.Text))
                    {
                            mInitBLE.Enabled = false;

                        //mBLEInterface.OnBLEAckReceived += new BLE_Interface.BLE_ReceiveEventHandler(BLE_);
                        mBLEInterface.OnBLEAckReceived += new BLE_Interface.BLE_ReceiveEventHandler(BLE_OnBLEAckReceived);
                        mBLEInterface.OnBLEDataReceived += new BLE_Interface.BLE_ReceiveEventHandler(BLE_OnBLEDataReceived);                       
                    }
                }

            }
        }

        private void mSendTest_Click(object sender, EventArgs e)
        {
            byte[] data = new byte[20];

            if (!mBLEInterface.m_bConnected)
            {
                mReceivedBytes.AppendText("BLE not initialized......\r\n");
                return;
            }

            data[0] = 0x73; // or 0x1F
            mBLEInterface.SendData(ref data, 1);
#if false
            //if (BLE_SendDataOnClicked != null)
            {
                //BLE_SendDataOnClicked(this, new BLEControlOnClickedArgs(BLE_number));

                if (!mBLEInterface.m_bConnected)
                {
                    mReceivedBytes.AppendText("BLE not initialized......\r\n");
                    return;
                }

                //m_receivedBytes = 0;
                //bool bLoopback = mDeviceShouldReturn.Checked;
                //ushort para2 = (ushort)((bLoopback/*|| (m_Test == TEST_MODE.TEST_PERFORMANCE_IN)*/) ? 1 : 0);
                //mBLEInterface.Data_SetDescriptors(1, para2);

                //mBLEInterface.SendCommand(1, 1);//(byte)NumFramesBeforeAck);



                int len;
                byte[] data = GetNextFrame(out len);

                //data[18] = 0x0d;
                //data[19] = 0x0a;
                mBLEInterface.SendData(ref data, len);
#endif
#if false
                int wait_count = 0;

                for (; ; )
                {

                    if (!oSignalEvent.WaitOne(10))
                    {
                        //ods("Ack failed\n");
                        wait_count++;
                        if (wait_count > 500)
                        {
                            wait_count = 1000;
                            break;
                        }
                        else
                            Application.DoEvents();
                    }
                    else
                    {
                        oSignalEvent.Reset();
                        break;
                    }
                }
                if (wait_count == 1000)
                    mReceivedBytes.AppendText("Data Sending fail.....\r\n");
                //else
                //    mReceivedBytes.AppendText("Data Sending OK.....\r\n");
#endif
        }
       
        private void nLed0_Click(object sender, EventArgs e)
        {

            if (!mBLEInterface.m_bConnected)
            {
                mReceivedBytes.AppendText("BLE not initialized......\r\n");
                return;
            }
            if (led0 == 0)
                led0 = 1;
            else
                led0 = 0;

            //mBLEInterface.LED_Control(0, led0);
            mBLEInterface.SendCommand((byte)'B', 1);// led0);
        }

        private void nLed1_Click(object sender, EventArgs e)
        {
#if false
            if (!mBLEInterface.m_bConnected)
            {
                mReceivedBytes.AppendText("BLE not initialized......\r\n");
                return;
            }
            if (led1 == 0)
                led1 = 1;
            else
                led1 = 0;
            mBLEInterface.LED_Control(1, led1);
#else
            if(nLed1.Text == "KeepLive")
            {
                nLed1.Text = "StopLive";
                mTimer.Enabled = true;

                mSendIn.Enabled = false;
                mZeroAngle.Enabled = false;
                mFlexLEDOnOff.Enabled = false;
                nLed0.Enabled = false;
                deviceCmdList.Enabled = false;
            }
            else
            {
                nLed1.Text = "KeepLive";
                mTimer.Enabled = false;

                mSendIn.Enabled = true;
                mZeroAngle.Enabled = true;
                mFlexLEDOnOff.Enabled = true;
                nLed0.Enabled = true;
                deviceCmdList.Enabled = true;
            }
#endif
        }

        private void mBT_Addr_SelectedIndexChanged(object sender, EventArgs e)
        {
            mBT_Name.SelectedIndex = mBT_Addr.SelectedIndex;
        }


        private void BLE_UserControl_Load(object sender, EventArgs e)
        {

        }

        private void mZeroAngle_Click(object sender, EventArgs e)
        {
            byte[] data = new byte[20];

            if (!mBLEInterface.m_bConnected)
            {
                mReceivedBytes.AppendText("BLE not initialized......\r\n");
                return;
            }

            data[0] = 0x73; // or 0x1F
            mBLEInterface.Data_SetDescriptors(1, 1);
            mBLEInterface.SendData(ref data, 1);
        }

        bool ledOnOff = true;
        private void mFlexLEDOnOff_Click(object sender, EventArgs e)
        {
            byte[] data = new byte[20];

            if (!mBLEInterface.m_bConnected)
            {
                mReceivedBytes.AppendText("BLE not initialized......\r\n");
                return;
            }


            if (ledOnOff)
                data[0] = 0x76; // or 0x1C
            else
                data[0] = 0x77; // or 0x1B
            ledOnOff = !ledOnOff;
            mBLEInterface.Data_SetDescriptors(1, 1);
            mBLEInterface.SendData(ref data, 1);
        }

        private void mClearAll_Click(object sender, EventArgs e)
        {
            mReceivedBytes.Clear();
            textBoxACK.Clear();
            m_receivedBytes = 0;
            mAckPackets = 0;

        }

        private void mTimer_Tick(object sender, EventArgs e)
        {
#if false
            byte[] data = new byte[20];

            if (!mBLEInterface.m_bConnected)
            {
                mReceivedBytes.AppendText("BLE not initialized......\r\n");
                return;
            }

            data[0] = 0x78; // or 0x1A
            //FrameRecv_disable = true;            
            //mBLEInterface.Data_SetDescriptors(1, 1);
            mBLEInterface.SendData(ref data, 1);
#else
            this.BeginInvoke((MethodInvoker)delegate { mSendIn_Click(sender, e); });
            //mSendIn_Click(sender, e);
#endif
        }

        //Thread th = Thread.

        private void cmdSend()
        {
            byte[] data = new byte[20];

            if (!mBLEInterface.m_bConnected)
            {
                mReceivedBytes.AppendText("BLE not initialized......\r\n");
                return;
            }

            int index = deviceCmdList.SelectedIndex;

            switch (index)
            {
                case 0:
                    data[0] = 0x75; // or 0x1D
                    break;
                case 1:
                    data[0] = 0x78; // or 0x1A
                    break;
                case 2:
                    data[0] = 0x74; // or 0x1E
                    break;
                case 3:
                    data[0] = 0x76; // or 0x1F
                    break;
                case 4:
                    data[0] = 0x77; // or 0x1C
                    break;
                default:
                    return;
                    //break;

            }
            //FrameRecv_disable = true;            
            mBLEInterface.Data_SetDescriptors(1, 1);
            //mBLEInterface.AfxData_SetDescriptors();
            mBLEInterface.SendData(ref data, 1);
        }

        private void mSendIn_Click(object sender, EventArgs e)
        {
#if true
            byte[] data = new byte[20];

            if (!mBLEInterface.m_bConnected)
            {
                mReceivedBytes.AppendText("BLE not initialized......\r\n");
                return;
            }

            int index = deviceCmdList.SelectedIndex;

            switch(index)
            {
                case 0:
                    data[0] = 0x75; // or 0x1D
                    break;
                case 1:
                    data[0] = 0x78; // or 0x1A
                    break;
                case 2:
                    data[0] = 0x74; // or 0x1E
                    break;
                case 3:
                    data[0] = 0x76; // or 0x1F
                    break;
                case 4:
                    data[0] = 0x77; // or 0x1C
                    break;
                default:
                    return;
                    //break;

            }
            //FrameRecv_disable = true;            
           // mBLEInterface.Data_SetDescriptors(1, 1);
            //mBLEInterface.AfxData_SetDescriptors();
            mBLEInterface.SendData(ref data, 1);            
            // FrameRecv_disable = false;
#else
            Thread send_threadx;
            send_threadx =  new Thread(send_cmd_job);
            send_threadx.Start();
#endif
        }
    }

    public class BLEControlOnClickedArgs : EventArgs
    {
        public readonly int BLE_Number;
        //public readonly Form BLE_Number;

        public BLEControlOnClickedArgs(int ble_number)//, Form pform)
        {
            BLE_Number = ble_number;
        }
    }

    public delegate void BLE_InitOnClickedEventHander(object sender, BLEControlOnClickedArgs args);
    //public delegate void BLE_DataModeOnClickedEventHandler(object sender, BLEControlOnClickedArgs args);
    //public delegate void BLE_StartInOnClickedEventHander(object sender, BLEControlOnClickedArgs args);
    //public delegate void BLE_SendDataOnClickedEventHander(object sender, BLEControlOnClickedArgs args);
    //public delegate void BLE_KeepSendingOnClickedEventHander(object sender, BLEControlOnClickedArgs args);


}
