﻿namespace BT_CYFI_Bridge_CSharp
{
    partial class BLE_UserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            Close();
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.mBT_Name = new System.Windows.Forms.ComboBox();
            this.mInitBLE = new System.Windows.Forms.Button();
            this.mBT_Addr = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.nLed1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.deviceCmdList = new System.Windows.Forms.ComboBox();
            this.mSendIn = new System.Windows.Forms.Button();
            this.mFlexLEDOnOff = new System.Windows.Forms.Button();
            this.nLed0 = new System.Windows.Forms.Button();
            this.mZeroAngle = new System.Windows.Forms.Button();
            this.mReceiveBox = new System.Windows.Forms.Label();
            this.mReceivedPackets = new System.Windows.Forms.Label();
            this.mClearAll = new System.Windows.Forms.Button();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxACK = new System.Windows.Forms.TextBox();
            this.mReceivedBytes = new System.Windows.Forms.TextBox();
            this.mTimer = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(381, 555);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42F));
            this.tableLayoutPanel2.Controls.Add(this.mBT_Name, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.mInitBLE, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.mBT_Addr, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(375, 29);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // mBT_Name
            // 
            this.mBT_Name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mBT_Name.FormattingEnabled = true;
            this.mBT_Name.Location = new System.Drawing.Point(220, 3);
            this.mBT_Name.Name = "mBT_Name";
            this.mBT_Name.Size = new System.Drawing.Size(152, 21);
            this.mBT_Name.TabIndex = 22;
            // 
            // mInitBLE
            // 
            this.mInitBLE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mInitBLE.Location = new System.Drawing.Point(3, 3);
            this.mInitBLE.Name = "mInitBLE";
            this.mInitBLE.Size = new System.Drawing.Size(80, 23);
            this.mInitBLE.TabIndex = 34;
            this.mInitBLE.Text = "Init BLE";
            this.mInitBLE.UseVisualStyleBackColor = true;
            this.mInitBLE.Click += new System.EventHandler(this.mInitBLE_Click);
            // 
            // mBT_Addr
            // 
            this.mBT_Addr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mBT_Addr.FormattingEnabled = true;
            this.mBT_Addr.Location = new System.Drawing.Point(89, 3);
            this.mBT_Addr.Name = "mBT_Addr";
            this.mBT_Addr.Size = new System.Drawing.Size(125, 21);
            this.mBT_Addr.TabIndex = 21;
            this.mBT_Addr.SelectedIndexChanged += new System.EventHandler(this.mBT_Addr_SelectedIndexChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel3.Controls.Add(this.nLed1, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel7, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.mFlexLEDOnOff, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.nLed0, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.mZeroAngle, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.mReceiveBox, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.mReceivedPackets, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.mClearAll, 3, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 478);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(375, 74);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // nLed1
            // 
            this.nLed1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nLed1.Location = new System.Drawing.Point(115, 40);
            this.nLed1.Name = "nLed1";
            this.nLed1.Size = new System.Drawing.Size(50, 31);
            this.nLed1.TabIndex = 120;
            this.nLed1.Text = "KeepLive";
            this.nLed1.UseVisualStyleBackColor = true;
            this.nLed1.Click += new System.EventHandler(this.nLed1_Click);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57F));
            this.tableLayoutPanel7.Controls.Add(this.deviceCmdList, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.mSendIn, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(171, 40);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(144, 31);
            this.tableLayoutPanel7.TabIndex = 123;
            // 
            // deviceCmdList
            // 
            this.deviceCmdList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deviceCmdList.FormattingEnabled = true;
            this.deviceCmdList.Items.AddRange(new object[] {
            "Power off",
            "KeepAlive",
            "Firmware Version",
            "Ch3 & Ch4 On",
            "Ch3 & Ch4 Off"});
            this.deviceCmdList.Location = new System.Drawing.Point(64, 3);
            this.deviceCmdList.Name = "deviceCmdList";
            this.deviceCmdList.Size = new System.Drawing.Size(77, 21);
            this.deviceCmdList.TabIndex = 112;
            // 
            // mSendIn
            // 
            this.mSendIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mSendIn.Location = new System.Drawing.Point(3, 3);
            this.mSendIn.Name = "mSendIn";
            this.mSendIn.Size = new System.Drawing.Size(55, 25);
            this.mSendIn.TabIndex = 111;
            this.mSendIn.Text = "Setup";
            this.mSendIn.UseVisualStyleBackColor = true;
            this.mSendIn.Click += new System.EventHandler(this.mSendIn_Click);
            // 
            // mFlexLEDOnOff
            // 
            this.mFlexLEDOnOff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mFlexLEDOnOff.Location = new System.Drawing.Point(3, 40);
            this.mFlexLEDOnOff.Name = "mFlexLEDOnOff";
            this.mFlexLEDOnOff.Size = new System.Drawing.Size(106, 31);
            this.mFlexLEDOnOff.TabIndex = 113;
            this.mFlexLEDOnOff.Text = "LED On/Off (Flex)";
            this.mFlexLEDOnOff.UseVisualStyleBackColor = true;
            this.mFlexLEDOnOff.Click += new System.EventHandler(this.mFlexLEDOnOff_Click);
            // 
            // nLed0
            // 
            this.nLed0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nLed0.Location = new System.Drawing.Point(115, 3);
            this.nLed0.Name = "nLed0";
            this.nLed0.Size = new System.Drawing.Size(50, 31);
            this.nLed0.TabIndex = 119;
            this.nLed0.Text = "LED0";
            this.nLed0.UseVisualStyleBackColor = true;
            this.nLed0.Click += new System.EventHandler(this.nLed0_Click);
            // 
            // mZeroAngle
            // 
            this.mZeroAngle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mZeroAngle.Location = new System.Drawing.Point(3, 3);
            this.mZeroAngle.Name = "mZeroAngle";
            this.mZeroAngle.Size = new System.Drawing.Size(106, 31);
            this.mZeroAngle.TabIndex = 124;
            this.mZeroAngle.Text = "Zero Angle (Flex)";
            this.mZeroAngle.UseVisualStyleBackColor = true;
            this.mZeroAngle.Click += new System.EventHandler(this.mZeroAngle_Click);
            // 
            // mReceiveBox
            // 
            this.mReceiveBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mReceiveBox.Location = new System.Drawing.Point(171, 0);
            this.mReceiveBox.Name = "mReceiveBox";
            this.mReceiveBox.Size = new System.Drawing.Size(144, 37);
            this.mReceiveBox.TabIndex = 122;
            this.mReceiveBox.Text = "00000000000000000";
            this.mReceiveBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mReceivedPackets
            // 
            this.mReceivedPackets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mReceivedPackets.Location = new System.Drawing.Point(321, 0);
            this.mReceivedPackets.Name = "mReceivedPackets";
            this.mReceivedPackets.Size = new System.Drawing.Size(51, 37);
            this.mReceivedPackets.TabIndex = 118;
            this.mReceivedPackets.Text = "0";
            this.mReceivedPackets.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mClearAll
            // 
            this.mClearAll.Location = new System.Drawing.Point(321, 40);
            this.mClearAll.Name = "mClearAll";
            this.mClearAll.Size = new System.Drawing.Size(51, 23);
            this.mClearAll.TabIndex = 125;
            this.mClearAll.Text = "Clear All";
            this.mClearAll.UseVisualStyleBackColor = true;
            this.mClearAll.Click += new System.EventHandler(this.mClearAll_Click);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.Controls.Add(this.textBoxACK, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.mReceivedBytes, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 38);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(375, 434);
            this.tableLayoutPanel5.TabIndex = 2;
            // 
            // textBoxACK
            // 
            this.textBoxACK.BackColor = System.Drawing.Color.Black;
            this.textBoxACK.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxACK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxACK.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBoxACK.Location = new System.Drawing.Point(284, 3);
            this.textBoxACK.Multiline = true;
            this.textBoxACK.Name = "textBoxACK";
            this.textBoxACK.Size = new System.Drawing.Size(88, 428);
            this.textBoxACK.TabIndex = 26;
            // 
            // mReceivedBytes
            // 
            this.mReceivedBytes.BackColor = System.Drawing.Color.Black;
            this.mReceivedBytes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.mReceivedBytes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mReceivedBytes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.mReceivedBytes.Location = new System.Drawing.Point(3, 3);
            this.mReceivedBytes.Multiline = true;
            this.mReceivedBytes.Name = "mReceivedBytes";
            this.mReceivedBytes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.mReceivedBytes.Size = new System.Drawing.Size(275, 428);
            this.mReceivedBytes.TabIndex = 25;
            // 
            // mTimer
            // 
            this.mTimer.Interval = 60000;
            this.mTimer.Tick += new System.EventHandler(this.mTimer_Tick);
            // 
            // BLE_UserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "BLE_UserControl";
            this.Size = new System.Drawing.Size(381, 555);
            this.Load += new System.EventHandler(this.BLE_UserControl_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox mReceivedBytes;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button mInitBLE;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button mSendIn;
        private System.Windows.Forms.Button mFlexLEDOnOff;
        private System.Windows.Forms.Label mReceivedPackets;
        private System.Windows.Forms.Button nLed1;
        private System.Windows.Forms.Button nLed0;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TextBox textBoxACK;
        private System.Windows.Forms.ComboBox mBT_Name;
        private System.Windows.Forms.ComboBox mBT_Addr;
        private System.Windows.Forms.Label mReceiveBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.ComboBox deviceCmdList;
        private System.Windows.Forms.Button mZeroAngle;
        private System.Windows.Forms.Timer mTimer;
        private System.Windows.Forms.Button mClearAll;
    }
}
